import Layout from '../components/layout'
import Head from 'next/head'
import Link from 'next/link'
import styles from '../components/detail.module.scss'
import CalendarSvg from '../components/CalendarSvg'
import DistanceSvg from '../components/DistanceSvg'
import SizeSvg from '../components/SizeSvg'
import DangerSvg from '../components/DangerSvg'
import Button from '../components/button'
import { useRouter } from 'next/router'
import { useState, useEffect } from 'react'
import {formatDate} from '../Lib/lib'

const APIKey = 'of9tcu8J7oY6WlweOUkcKuecgOoOKdJ9TE8OiNEl';

function AsteroidInfo({post}) {
    
    const [list, setList] = useState([])
    
    useEffect(() => {
        if(!JSON.parse(localStorage.getItem('destroyList'))) return;
        setList(JSON.parse(localStorage.getItem('destroyList')))
    }, [])
    
    const router = useRouter()
    const {
        query: {...destroyList}
    } = router
    
    const {
        'is_potentially_hazardous_asteroid': danger, name, id,
        'name_limited': shortName,
        'orbital_data': orb, 'last_observation_date': lastday, 
        'close_approach_data': close, 'miss_distance': dist,
        'kilometers': km, 'estimated_diameter': diam, 'meters': m,
        'estimated_diameter_max': diammax
    } = post;

    function handleDestroyList(e) {
        e.preventDefault();
        setList([...list, post])
        localStorage.setItem('destroyList', JSON.stringify(list))
        console.log('destroyList: ', localStorage.destroyList);
    }
    
    return (
        <Layout>
            <Head>
                <title>{name}</title>
            </Head>
            <div className={styles.headerMenu}>
                <Link href={'/'}>
                    <a><h2>Астероиды</h2></a>
                </Link>
                <Link href={{
                    pathname: './destroy',
                    query: { destroyList: JSON.stringify(destroyList) }
                }} as='./destroy_list'>
                    <a>Уничтожение</a>
                </Link>
            </div>
            <div key={id} className={!danger ? styles.asteroidBlock : styles.dangerous}>
            <div className={styles.paramsBlock}>
                <div className={styles.titleBlock}>
                    <h2 title={name}>{name}</h2>
                    <div className={styles.params}>
                        <div className={styles.param} title='Ближайшая дата'>
                            <CalendarSvg />
                            <p>{formatDate(orb['last_observation_date'])}</p>
                        </div>
                        <div className={styles.param} title='Максимальное приближение'>
                            <DistanceSvg />
                            <p>{parseInt(close[0]['miss_distance']['kilometers']).toFixed(0)} км</p>
                        </div>
                        <div className={styles.param} title='Размер'>
                            <SizeSvg />
                            <p>{diam['meters']['estimated_diameter_max'].toFixed(0)} м</p>
                        </div>
                        {danger ?
                        <div title='Опасность'>
                            <DangerSvg />
                        </div>
                        : null}
                        <div className={styles.dangerBlock}>
                            <Button 
                            children
                            title='На уничтожение'
                            destroy={handleDestroyList}
                            />
                        </div>
                    </div>
                </div>
                <h3 className={styles.detailsTitle}>Сближения {shortName} с Землей</h3>
                
                <div className={styles.detailsBlock}>
                    
                    {close.map((it, index) => 
                        {
                            const planetsRus = () => {
                                if(it['orbiting_body'] === 'Earth') {return 'Земля'}
                                else if(it['orbiting_body'] === 'Juptr') {return 'Юпитер'}
                                else if(it['orbiting_body'] === 'Merc') {return 'Меркурий'}
                                else if(it['orbiting_body'] === 'Mars') {return 'Марс'}
                                else if(it['orbiting_body'] === 'Venus') {return 'Венера'}
                                else {return 'Далеко в космосе'}
                            }
                            return(
                            <div key={index} className={styles.details}>
                                <div className={styles.detail}>
                                    <h4>скорость относительно Земли</h4>
                                    <p>{parseInt(it['relative_velocity']['kilometers_per_hour']).toFixed(0)} км/ч</p>
                                </div>
                                <div className={styles.detail}>
                                    <h4>время максимального сближения с Землей</h4>
                                    <p>{new Date(it['close_approach_date_full']).toLocaleString()}</p>
                                </div>
                                <div className={styles.detail}>
                                    <h4>расстояние до Земли</h4>
                                    <p>{parseInt(it['miss_distance']['kilometers']).toFixed(0)} км</p>
                                </div>
                                <div className={styles.detail}>
                                    <h4>Орбита</h4>
                                    <p>{planetsRus()}</p>
                                </div>
                            </div>
                        )}
                    )}
                </div>
                </div>
            </div> 
        </Layout>
    );
};

export async function getStaticPaths() {
    const res = await fetch(`https://api.nasa.gov/neo/rest/v1/neo/browse?api_key=${APIKey}`)
    const posts = await res.json()
    
    const paths = posts['near_earth_objects'].map((post) => ({
        params: { id: post.id },
      }))
    return { paths, fallback: false }
}

export async function getStaticProps({ params }) {
    const res = await fetch(`https://api.nasa.gov/neo/rest/v1/neo/${params.id}?api_key=${APIKey}`)
    const post = await res.json()
    return { props: { post }  }
}

export default AsteroidInfo;
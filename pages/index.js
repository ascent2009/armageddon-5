import React from 'react'
import loadable from '@loadable/component'
import Head from 'next/head'
import Layout, {siteTitle} from '../components/layout'
// import Asteroid from '../components/asteroid'
const Asteroid = loadable(() => import('../components/asteroid'));

export async function getStaticProps() {
  const APIKey = 'of9tcu8J7oY6WlweOUkcKuecgOoOKdJ9TE8OiNEl';
  const res = await fetch(`https://api.nasa.gov/neo/rest/v1/neo/browse?api_key=${APIKey}`)
  const data = await res.json()
  return { props: { data } }
}

function HomePage({data}) {
  
  return (
      <Layout>
        <Head>
          <title>{siteTitle}</title>
        </Head>
        <main>
            <div>
              <Asteroid {...data} />
            </div>
        </main>
      </Layout>)
  }
  
  export default HomePage
import Layout from '../components/layout'
import Head from 'next/head'
import Link from 'next/link'
import { useState } from 'react'
import styles from '../components/asteroid.module.scss'
import Button from '../components/button'
import ExplosionAnimation from '../components/ExplosionAnimation'
import DinoSvg from '../components/DinoSvg'
import AsteroidSvg from '../components/AsteroidSvg'
import {str, formatDate} from '../Lib/lib'

export default function DestroyAsteroid({destroyList}) {
    const [open, setOpen] = useState(false)
    const [list, setList] = useState(JSON.parse(localStorage.getItem('destroyList')))
    
    function deleteDestroyList () {
        localStorage.removeItem('destroyList')
        setList(null)
        setOpen(true)
    }

    console.log(JSON.parse(localStorage.getItem('destroyList')));

    return (
        <Layout>
            <Head>
                <title>Asteroid {destroyList.name} to shoot down</title>
            </Head>
            <div className={styles.headerMenu}>
                <Link href={'/'}>
                    <a><h2>Астероиды</h2></a>
                </Link>
                <Link href={{
                    pathname: './destroy',
                    query: { destroyList: JSON.stringify(destroyList) }
                }} as='./destroy_list'>
                    <a>Уничтожение</a>
                </Link>
            </div>
            {!list ? '' : list.filter(it => it !== null).map(item => {
                const {
                    'is_potentially_hazardous_asteroid': danger, name, id,
                    'orbital_data': orb, 'last_observation_date': lastday, 
                    'close_approach_data': close, 'miss_distance': dist,
                    'kilometers': km, 'estimated_diameter': diam, 'meters': m,
                    'estimated_diameter_max': diammax
                } = item;
                
            return(
            <Link href={`/${id}`} key={id} >
            <div key={id} className={!danger ? styles.asteroidBlock : styles.dangerous} title='Кликни, чтобы посмотреть подробности'>
            <div className={!danger ? styles.imageBlock : styles.mobileDanger}>
                <DinoSvg />
                <AsteroidSvg />
            </div>
                <div className={styles.paramsBlock}>
                    <h2 title={name}>{name}</h2>
                    <div className={styles.params}>
                        <div className={styles.date}>
                            <p>Дата</p>
                            <div>{str}</div>
                            <p>{formatDate(orb['last_observation_date'])}</p>
                        </div>
                        <div className={styles.distance}>
                            <p>Расстояние</p>
                            <div>{str}</div>
                            <p>{parseInt(close[0]['miss_distance']['kilometers']).toFixed(0)} км</p>:
                        </div>
                        <div className={styles.size}>
                            <p>Размер</p>
                            <div>{str}</div>
                            <p>{diam['meters']['estimated_diameter_max'].toFixed(0)} м</p>
                        </div>
                    </div>
                </div>
            </div>
            </Link>)}
            )}
            <div className={styles.buttonBlock}>
                {!list ?
                <h2 className={styles.reportTitle}>Ничего не выбрано или все выбранные астероиды уничтожены</h2>
                : <Button
                    destroy={deleteDestroyList} 
                    title='Вызвать бригаду Брюса Уиллиса и уничтожить' />}
            </div>
            <ExplosionAnimation
                isOpen={open}
            />
        </Layout>
    );
};

DestroyAsteroid.getInitialProps = ({query: {destroyList} }) => {
    return {destroyList}
}

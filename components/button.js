import React from 'react'
import styles from './button.module.scss'

const Button = (props) => {
    const {destroy, id, title} = props;
    return (
        <button className={styles.button} onClick={destroy} data-id={id}>
             {/* {children} */}
             <p>{title}</p>
        </button>
    );
}

export default Button;
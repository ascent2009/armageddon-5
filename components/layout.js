import Head from 'next/head'
import Link from 'next/link'
import styles from './layout.module.scss'

export const siteTitle = 'ARMAGGEDON V: asteroid monitoring service'

export default function Layout({children}) {
    return (
        <div className={styles.container}>
            <Head>
                <link rel="icon" href="/comet.ico" />
                <meta
                name="description"
                content="Learn how to build a personal website using Next.js"
                />
                <meta
                property="og:image"
                content={`https://og-image.vercel.app/${encodeURI(
                    siteTitle
                )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
                />
                <meta name="og:title" content={siteTitle} />
                <meta name="twitter:card" content="summary_large_image" />
            </Head>
            <header className={styles.header}>
                <div className={styles.headerTitle}>
                    <h1>ARMAGGEDON V</h1>
                    <p>Сервис мониторинга и уничтожения астероидов, опасно подлетающих к Земле.</p>
                </div>
            </header>
            <main className={styles.main}>
                {children}
            </main>
            <footer className={styles.footer}>
                <p>2021 &copy; Все права и планета защищены</p>
            </footer>
        </div>
    )
}
import React from "react"

export default function SizeSvg() {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="32px" height="32px" version="1.1" viewBox="0 0 500 500">
 <defs>
  <style type="text/css">
   
  </style>
  <linearGradient id="id0" gradientUnits="userSpaceOnUse" x1="30.002" y1="297.143" x2="469.997" y2="297.143">
   <stop offset="0"/>
   <stop offset="0.501961"/>
   <stop offset="1"/>
  </linearGradient>
  <linearGradient id="id1" gradientUnits="userSpaceOnUse" x1="30.002" y1="202.857" x2="469.997" y2="202.857">
   <stop offset="0" />
   <stop offset="0.521569"/>
   <stop offset="1"/>
  </linearGradient>
  <linearGradient id="id2" gradientUnits="userSpaceOnUse" x1="250" y1="181.17" x2="250" y2="318.83">
   <stop offset="0"/>
   <stop offset="1"/>
  </linearGradient>
 </defs>
 <g id="Layer_x0020_1">
  <metadata id="CorelCorpID_0Corel-Layer"/>
  <path class="fil0" d="M472 266l-80 80c-4,4 -8,8 -16,8 -8,0 -16,-8 -16,-16l0 -40c0,-4 -4,-8 -8,-8 -68,0 -136,0 -204,0 -4,0 -8,4 -8,8l0 40c0,8 -8,16 -16,16 -8,0 -12,-4 -16,-8l-80 -80c-5,-5 -8,-11 -8,-16 0,-5 3,-11 8,-16l80 -80c4,-4 8,-8 16,-8 8,0 16,8 16,16l0 40c0,4 4,8 8,8 68,0 136,0 204,0 4,0 8,-4 8,-8l0 -40c0,-8 8,-16 16,-16 8,0 12,4 16,8l80 80c5,5 8,11 8,16 0,5 -3,11 -8,16z"/>
  <g id="_1325575056">
   <path class="fil1" d="M470 250l-440 0 0 0c0,-3 2,-6 5,-9l0 0 80 -80 0 0 0 0c3,-3 5,-5 9,-5 2,0 3,1 4,2l0 0c2,1 2,3 2,4l0 0 0 40 0 0 0 0c0,5 3,9 6,12l0 0c3,4 8,6 12,6l0 0 204 0 0 0c4,0 9,-2 12,-6 3,-3 6,-7 6,-12l0 0 0 0 0 -40 0 0c0,-1 0,-3 2,-4l0 0c1,-1 2,-2 4,-2 4,0 6,2 9,5l0 0 0 0 80 80 0 0c2,2 3,4 4,6 1,1 1,2 1,3l0 0z"/>
   <path class="fil2" d="M30 250l440 0c0,3 -2,6 -5,9l0 0 -80 80 0 0 0 0c-3,3 -5,5 -9,5 -2,0 -3,-1 -4,-2 -2,-1 -2,-3 -2,-4l0 0 0 -40 0 0 0 0 0 0c0,-5 -3,-9 -6,-12l0 0c-3,-4 -8,-6 -12,-6l0 0 -204 0 0 0c-4,0 -9,2 -12,6 -3,3 -6,7 -6,12l0 0 0 0 0 0 0 40 0 0c0,1 0,3 -2,4 -1,1 -2,2 -4,2 -4,0 -6,-2 -9,-5l0 0 0 0 -80 -80 0 0c-2,-2 -3,-4 -4,-6 -1,-1 -1,-2 -1,-3z"/>
  </g>
 </g>
</svg>

    )
}
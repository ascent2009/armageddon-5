import React from 'react'
import ReactDOM from 'react-dom'
import Image from 'next/image'
import styles from './explosion.module.scss'

const ExplosionAnimation = ({ isOpen})=> {
    
    if (!isOpen) return null;
    return ReactDOM.createPortal(
    <div className={styles.modalBlock}>
        <Image
          src='/images/explosion.png'
          alt="Взрыв"
          width={500}
          height={500} />
    </div>
      ,document.body);
    }

export default ExplosionAnimation;
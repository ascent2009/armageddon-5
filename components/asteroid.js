import React, {useState, useEffect, useRef} from 'react'
import Link from 'next/link'
import Button from '../components/button'
import DinoSvg from '../components/DinoSvg'
import AsteroidSvg from '../components/AsteroidSvg'
import utilStyles from '../styles/utils.module.scss'
import styles from './asteroid.module.scss'

export default function Asteroid(data) {
    
    const str = 'Lorem'.repeat(10);
    const itemRef = useRef(null)
    const arrData = data['near_earth_objects'];
    const {id, name, date, distance, size} = arrData;
    const [checked, setChecked] = useState(false)
    const [isLunar, setIsLunar] = useState(false)
    const [destroyList, setDestroyList] = useState([])
    
    useEffect(() => {
        if(!JSON.parse(localStorage.getItem('destroyList'))) return;
        setDestroyList(JSON.parse(localStorage.getItem('destroyList')))
    }, [])

    const handleDistanceUnits = () => {
        if(!isLunar) {
            setIsLunar(true)
        } else {
            setIsLunar(false)
        }
    }

    const handleCheckbox = () => {
        setChecked(!checked)
        console.log('checked: ', checked);
    }

    function filterData(x) {
        let y = x.filter(i => checked === i['is_potentially_hazardous_asteroid'])
        if (!checked) {return x}    
        return y;
    }

    function handleDestroyList(e) {
        e.preventDefault();
        const id = e.target.dataset.id;
        const item = arrData.find(item => id === item.id)
        setDestroyList([...destroyList, item])
        localStorage.setItem('destroyList', JSON.stringify(destroyList))
        console.log('destroyList: ', localStorage.destroyList);
    }

    return (
        <>
        <div className={styles.headerMenu}>
            <Link href={'/'}>
                <a><h2>Астероиды</h2></a>
            </Link>
            <Link href={{
                pathname: './destroy',
                query: { destroyList: JSON.stringify(destroyList) }
            }} as='./destroy_list'>
                <a>Уничтожение</a>
            </Link>
        </div>
        <div className={utilStyles.formBlock}>
            <form className={utilStyles.form}>
              <input type="checkbox" id="checkbox" className={utilStyles.checkbox} 
              onChange={handleCheckbox}
              />
              <label htmlFor="checkbox" className={utilStyles.label}>Показать только опасные</label>
            </form>
            <div className={utilStyles.distance}>
              <p>Расстояние в <span>километрах</span>,</p>
              <button onClick={handleDistanceUnits} className={utilStyles.moonDist}>в дистанциях до луны</button>
            </div>
        </div>
        {filterData(arrData)
            .map(item => {
                const {
                    'is_potentially_hazardous_asteroid': danger, name, id,
                    'orbital_data': orb, 'last_observation_date': lastday, 
                    'close_approach_data': close, 'miss_distance': dist,
                    'kilometers': km, 'estimated_diameter': diam, 'meters': m,
                    'estimated_diameter_max': diammax
            } = item;
            
            const formatDate = (x) => {
                let date = new Date(x).getTime();
                let date2 = new Date(date)
                    .toLocaleString()
                    .split(',')[0]
                    .replace(/[.]/g, '-');
                 return date2;
            }
                      
            
            return(
            <Link href={`/${id}` } {...arrData} key={id} >
            <div key={id} className={!danger ? styles.asteroidBlock : styles.dangerous} ref={itemRef} title='Кликни, чтобы посмотреть подробности'>
            <div className={!danger ? styles.imageBlock : styles.mobileDanger}>
                <DinoSvg />
                <AsteroidSvg />
            </div>
            <div className={styles.paramsBlock}>
                <h2 title={name}>{name}</h2>
                <div className={styles.params}>
                    <div className={styles.date}>
                        <p>Дата</p>
                        <div>{str}</div>
                        <p>{formatDate(orb['last_observation_date'])}</p>
                    </div>
                    <div className={styles.distance}>
                        <p>Расстояние</p>
                        <div>{str}</div>
                        {!isLunar ?
                            <p>{parseInt(close[0]['miss_distance']['kilometers']).toFixed(0)} км</p>:
                            <p>{parseInt(close[0]['miss_distance']['lunar']).toFixed(2)} LD</p>
                        }
                    </div>
                    <div className={styles.size}>
                        <p>Размер</p>
                        <div>{str}</div>
                        <p>{diam['meters']['estimated_diameter_max'].toFixed(0)} м</p>
                    </div>
                </div>
            </div>
            <div className={styles.dangerBlock}>
                <p>Оценка:<br />
                {!danger ?
                <span>не опасен</span> : <span>опасен</span>}
                </p>
                <Button 
                    title='На уничтожение'
                    id={id}
                    arrData={arrData}
                    destroy={handleDestroyList}
                />
               </div>
            </div>
 
            </Link>
            )
            }
            )}
        </>
    );
}
export const str = 'Lorem'.repeat(10);

export const formatDate = (x) => {
  let date = new Date(x).getTime();
  let date2 = new Date(date)
      .toLocaleString()
      .split(',')[0]
      .replace(/[.]/g, '-');
   return date2;
}